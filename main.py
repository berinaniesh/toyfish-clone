import json

class Chess:

    def __init__(self, filename):
        with open(filename) as f:
            self.__dict__ = json.loads(f.read())
            self.board = list('        \n' * 2 + '' + ''.join([
                              '.' * int(c) if c.isdigit() else c for c in self.fen.split()[0].replace('/', '\n')
                              ]) + '\n' + '       \n' * 2)
            self.side = 0 if self.fen.split()[1] == 'w' else 1

    def generate_moves(self):
        for square in range(len(self.board)):
            piece = self.board[square]
            if piece not in ' .\n' and self.colors[piece] == self.side:
                for offset in self.directions[piece]:
                    target_square = square
                    while True:
                        target_square += offset
                        captured_piece = self.board[target_square]
                        if captured_piece in ' \n': break
                        self.board[target_square] =  piece
                        self.board[square] = '.'
                        print(''.join([' ' + chess.pieces[p] for p in ''.join(chess.board)]), chess.side); input
                        self.board[target_square] = captured_piece
                        self.board[square] = piece
                        print(''.join([' ' + chess.pieces[p] for p in ''.join(chess.board)]), chess.side); input


chess = Chess('settings.json')

print(''.join([' ' + chess.pieces[p] for p in ''.join(chess.board)]), chess.side)

chess.generate_moves()
